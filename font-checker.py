#!/usr/bin/env python
from itertools import chain
import sys
import os

from fontTools.ttLib import TTFont
from fontTools.unicode import Unicode

CHAR_LIST_FILE = 'char-list'
FONT_DIR = './fonts/'

args_provided = len(sys.argv) == 2

if __name__ == '__main__':
    if args_provided:
        font_name = sys.argv[1]
    else:
        font_name = input('Enter the font name> ')
    ttf = TTFont(os.path.join(FONT_DIR, font_name), 0,
                 allowVID=0,
                 ignoreDecompileErrors=True,
                 fontNumber=-1)

    font_chars = chain.from_iterable([y + (Unicode[y[0]],) for y in x.cmap.items()] for x in ttf["cmap"].tables)
    font_chars = [x[0] for x in font_chars]


    with open(CHAR_LIST_FILE, 'r', encoding='utf8') as fd:
        required_chars = fd.read().split()

    total_present = 0
    for char in required_chars:
        ord_char = ord(char)
        is_present = ord_char in font_chars
        if is_present:
            total_present += 1
        print('{:2} {:35} {}'.format(char, Unicode[ord_char], is_present))

    ttf.close()

    print('{} out of {} characters present ({:.2f}%)'.format(total_present, len(required_chars), (total_present / len(required_chars)) * 100))
    if not args_provided:
        input()
