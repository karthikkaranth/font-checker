# Find all the unique characters in a file

import sys

if len(sys.argv) == 2:
    fd = open(sys.argv[1], 'r')
else:
    fd = sys.stdin

unique_chars = set()

for line in fd:
    s = eval('"""' + line.rstrip() + '"""')   
    for letter in s:
        unique_chars.add(letter)

for char in unique_chars:
    print(char, end=' ')
