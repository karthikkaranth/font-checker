# Parse rqworker log to get list of CTA

import sys
import re
import json

if len(sys.argv) == 2:
    fd = open(sys.argv[1], 'r')
else:
    fd = sys.stdin

regex = re.compile(r"'(.*)'")

for line in fd:
    match = regex.search(line)
    if match:
        try:
            json_str = match.group(1)
            d = json.loads(json_str)
            resources = d['resources']

            for field in resources:
                if field['name'] == 'cta':
                    print(field['value'])
                    break
        except Exception as e:
            pass
